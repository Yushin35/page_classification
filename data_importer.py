from sklearn.datasets import fetch_20newsgroups


def get_data_list():
    return fetch_20newsgroups(
            shuffle=True, 
            random_state=1, 
            remove=('headers', 'footers', 'quotes'),
            ).data
