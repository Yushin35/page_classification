from collections import defaultdict

import numpy as np

from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.decomposition import NMF, LatentDirichletAllocation

import nltk
from nltk.corpus import stopwords

from data_importer import get_data_list


no_features = 1000

nltk.download('stopwords')
stopWords = set(stopwords.words('english'))


documents = get_data_list()


count_vectorizer = CountVectorizer(max_df=0.95, 
                                   min_df=2,
                                   max_features=no_features, 
                                   stop_words=stopWords)
words_matrix = count_vectorizer.fit_transform(documents)
words_matrix_feature_names = count_vectorizer.get_feature_names()

no_topics = 20

# It can also be viewed as distribution over 
# the words for each topic after normalization
lda = LatentDirichletAllocation(
        n_components=no_topics, 
        max_iter=5, 
        learning_method='online', 
        learning_offset=50.,
        random_state=0).fit(words_matrix)



def get_topics_words_dict(model, feature_names, no_top_words):
    topics_words_dict = defaultdict(list)
    for topic_idx, topic in enumerate(model.components_):
        for i in topic.argsort()[:-no_top_words - 1:-1]:
            topics_words_dict[topic_idx].append(feature_names[i])
    return topics_words_dict


def get_document_topic_words(document_index, model, words_matrix, topics_words_dict):
    res_array = model.transform(words_matrix[document_index])[0]
    return topics_words_dict.get(np.argmax(res_array))



no_top_words = 10
topics_words_dict = get_topics_words_dict(lda, words_matrix_feature_names, no_top_words)

#print(topics_words_dict)
#print(lda.transform(words_matrix))


for i in range(10):
    print('$'*30)
    print('DOCS', documents[i], 'TOPIC', get_document_topic_words(i, lda, words_matrix, topics_words_dict))

